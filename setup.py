# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
import os.path
import sys



PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))

base = None
icone = None
here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='sonarqube',
    version='1.0.0',
    description='A python package sample',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Dom31',
    author_email='toutpres@yahoo.fr',
    license="MIT",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    keywords='python package template documentation testing',
    packages=find_packages('src',exclude=['docs', 'tests','data','build','htmlcov']),
    package_dir={'': 'python_code/src'},
    include_package_data=True,
    zip_safe=False,
    setup_requires=['pytest-runner', 'setuptools>=38.6.0'],  # >38.6.0 needed for markdown README.md
    tests_require=['pytest', 'pytest-cov'],
    scripts=[
        'bash_tools/checkEnv',
        'bash_tools/lib_bash_functions.sh',
        'bash_tools/checkPython'
    ]
)
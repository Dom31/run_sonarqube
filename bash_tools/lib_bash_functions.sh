#!/bin/bash
#########################################################################################
# Linux bash functions used as a library
#########################################################################################



lineSep="--------------------------------------------------------------------------------"
# debug output (short vars to facilitate the display of debug messages)
F=FUNCNAME
L=LINENO
logLevel="info"                          # information displayed (no debug mode, no quiet mode)


#######################################
# display messages for information
# In Globals: logLevel logboog
# Arguments: message
#######################################
function info 
{

  if [ "$logLevel" == "info" -o "$logLevel" == "debug" ] ; then
    #echo "[$scriptName] : $@"| tee -a $logbook
    echo -e "\e[92m$@\e[0m" | tee -a $logbook 
    #echo "$@" | tee -a $logbook
  fi

}
#######################################
# display messages for debug
# In Globals: logLevel logbook
# Arguments: title + message
#######################################
function debug 
{
  local title=$1
  shift
  if [ "$logLevel" == "debug" ] ; then
    echo "[DEBUG $title]: $@" | tee -a $logbook
  else
    echo "[DEBUG $title]: $@" >> $logbook
  fi
}

#######################################
# display error messages on &2 and exit (code  = 1)
# In Globals: scriptName logLevel logbook
# Arguments: title + message
#######################################
function error 
{
  local title=$1
  shift
  #echo "[ERROR $scriptName line $title]: $@" | tee -a $logbook 
  echo -e "\e[91m[ERROR $scriptName line $title]: $@\e[0m" | tee -a $logbook
  exit 1
}

#######################################
# display warning messages on &2
# In Globals: scriptName logLevel logbook
# Arguments: title + message
#######################################
function warn 
{
    local title=$1
    shift
    #echo "[WARN $scriptName line $title]: $@" | tee -a $logbook
    echo -e "\e[91m[WARN $scriptName line $title]: $@\e[0m" | tee -a $logbook
}

#######################################
# display addscript lines 
# In Globals: runToolsScript
# Arguments: addscript line
#######################################
function addscript 
{
    #echo "[WARN $scriptName line $title]: $@" | tee -a $logbook
    echo -e "$@" >> $runToolsScript
}

#######################################
# existsAndNotEmpty 
# Arguments: directory or file
# return TRUE (0) if file/directory exists and is not empty else returns FALSE (1)
#######################################
function existsAndNotEmpty  
{
  local item=$1
  debug ${!F}_${!L} "----------------> BEGIN: existsAndNotEmpty $item"
  local returnCode=1 # FALSE
  if [ -f "$item" ] ; then
    [ -s "$item" ] && returnCode=0 # TRUE: file exists and not empty
  elif [ -d "$item" ] ; then
    [ "$(ls -A $item)" ] && returnCode=0 # TRUE: directory exists and contains file(s)
  fi
  debug ${!F}_${!L} "----------------> END: existsAndNotEmpty with return code=$returnCode"
  return $returnCode
} # existsAndNotEmpty

#######################################
# initialize addscript lines script
# In Globals: runToolsScript
#######################################
function initRunToolsScript
{
  debug ${!F}_${!L} "touch $runToolsScript"
  rm $runToolsScript 2>/dev/null
  touch $runToolsScript
  chmod a+x $runToolsScript
  addscript "#!/bin/bash"
}
withTmpOption=5

######################################
# initialize logbook
# In Globals: logbook reportsDir reportsDirTMP
# Arguments: "tmp" or "append" (optional $1)
#######################################
function initLogbook
{



  if [[ "$withTmpOption" == "tmp" ]] ; then
    # shell run with only "display" commands (no persistent logbook) 
    reportsDir=$reportsDirTMP
    logbook=${reportsDir}/${scriptName}_report.txt
    mkdir ${reportsDir} 2>/dev/null

    # initialize the logbook for next messages (info, debug, warn, error)
    touch $logbook
  else
   if [[ "$withTmpOption" == "append" ]] && [[ -d $reportsDir ]] ; then
      # echo "The folder ${reportsDir} exists and will be used by the command with \"$withTmpOption\""
      debug ${!F}_${!L} "The folder ${reportsDir} exists and will be used by the command with \"$withTmpOption\""
  local withTmpOption=$1
     else
    # persistent logbook: save the previous before launching commands
    # echo "${reportsDir} deleted"
    rm -rf ${reportsDir}/${reportsDir}_previous 2>/dev/null
    #read -p "rm -rf ${reportsDir}/${reportsDir}_previous"
    mv ${reportsDir} ${reportsDir}_previous 2>/dev/null
    #read -p "mv ${reportsDir} ${reportsDir}_previous"
    mkdir ${reportsDir} 2>/dev/null
    #read -p "mkdir ${reportsDir}"


    mv ${reportsDir}_previous ${reportsDir} 2>/dev/null
    #read -p "mv ${reportsDir}_previous ${reportsDir} "
    cp sonar-project.properties ${reportsDir}/${reportsDir}_previous 2>/dev/null

    # initialize the logbook for next messages (info, debug, warn, error)
    touch $logbook
    #read -p "touch $logbook"
    debug ${!F}_${!L} "The previous folder ${reportsDir} has been moved in ${reportsDir}/${reportsDir}_previous"
   fi
  fi

} # initLogbook

#######################################
# exit script (run with "display" commands, i.e. no persistent logbook)
# In Globals: reportsDirTMP
#######################################
function exitScript
{
  rm -rf ${reportsDir} 2>/dev/null
  exit 1
} # exitScript
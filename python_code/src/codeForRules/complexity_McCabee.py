# http://pylint.pycqa.org/en/latest/technical_reference/extensions.html
# $ pylint complexity_McCabee.py --load-plugins=pylint.extensions.mccabe
# R:1: 'mac_cabee' is too complex. The McCabe rating is 11 (too-complex)
# $ pylint a.py --load-plugins=pylint.extensions.mccabe --max-complexity=50

def mac_cabee():
    """McCabe rating: 11"""
    myint = 2
    if myint == 5:
        return myint
    elif myint == 6:
        return myint
    elif myint == 7:
        return myint
    elif myint == 8:
        return myint
    elif myint == 9:
        return myint
    elif myint == 10:
        if myint == 8:
            while True:
                return True
        elif myint == 8:
            with myint:
                return 8
    else:
        if myint == 2:
            return myint
        return myint
    return myint
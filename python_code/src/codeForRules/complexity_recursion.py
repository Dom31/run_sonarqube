# Python module for checking the complexities (cyclomatic, cognitive)
# Source: https://www.tomasvotruba.cz/blog/2018/05/21/is-your-code-readable-by-humans-cognitive-complexity-tells-you/

# Example A
# Cyclomatic Complexity: 4

def f(a):
    result = a * f(a - 1)  # +1 for recursion
    return result

def g(a, b):
    if a:
        for i in range(b):
            if b:
                return 1 # cognitive complexity is 6



# Pylint:R0915:Too many statements
# Pylint:R0902:Too many instance attributes
# Pylint:R0914:Too many local variables
# python:S107: Functions, methods and lambdas should not have too many parameters

class myclass(object):
    def func1(self,v1,v2,v3,v4,v5,v6,v7):
        self.instance_variafble1 = 1

    def func2(self, v1,v2,v3,v4,v5,v6):
        self.a1 = 1
        self.a2 = 1
        self.a3 = 1
        self.a4 = 1
        self.a5 = 1
        self.a6 = 1
        self.a7 = 1
        self.a8 = 1
        self.a9 = 1
        self.a10 = 1

        self.b1 = 1
        self.b2 = 1
        self.b3 = 1
        self.b4 = 1
        self.b5 = 1
        self.b6 = 1
        self.b7 = 1
        self.b8 = 1
        self.b9 = 1
        self.b10 = 1

        self.c1 = 1
        self.c2 = 1
        self.c3 = 1
        self.c4 = 1
        self.c5 = 1
        self.c6 = 1
        self.c7 = 1
        self.c8 = 1
        self.c9 = 1
        self.c10 = 1

        self.more_than_30_attributes = 1

        d1 = 1
        d2 = 1
        d3 = 1
        d4 = 1
        d5 = 1
        d6 = 1
        d7 = 1
        d8 = 1
        d9 = 1
        d10 = 1
        d11 = 1
        d12 = 1
        d13 = 1
        d14 = 1
        d15 = 1 # 15 local variables
        d15 = 1
        d17 = 1
        d18 = 1
        d19 = 1
        d20 = 1
        d21 = 1
        d22 = 1
        d23 = 1
        d24 = 1
        d25 = 1 # 56 lines of code and statements







"""
module: names

PEP8: Modules should have short, all-lowercase names. Underscores can be used in the module name if it improves readability.
"""

# PEP8: Names to Avoid Never use the characters 'l' (lowercase letter el), 'O' (uppercase letter oh), or 'I' (uppercase letter eye) as single character variable names.
# Avoid as possible global variables
# Constants are usually defined on a module level and written in all capital letters with underscores separating words
# =====================================

MY_GLOBAL_CONST = 10
my_global_const = 10
l = 10

# Scoping: namespaces in python
# ==============================

my_var = 11                       # Global (module) name/attribute (my_var, or names.my_var)

def foo():
    print(my_var)                 # Access global my_var (11)


def bar():
    my_var = 22                   # Local (function) variable (my_var, hides module my_var)
    print(my_var)


class MyClass(object):
    my_var = 33                   # Class attribute (MyClass.my_var)

    def foo(self):
        my_var = 44               # Local variable in method (my_var)
        self.my_var = 55          # Instance attribute (instance.my_var)

# Class names use mixed case starting with uppercase
# Classes for internal use MUST have a leading underscore
# An exception name MUST include the suffix "Error"
# ======================================================


class _Internal(object):
    a = 1


class MyCustomError(Exception):
    a = 1


# Public, Protected and Private attributes
# =========================================

class Mapping(object):
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)        # private attribute (variable)
        self._update_status = True      # protected attribute

    def update(self, iterable):        # public attribute (method)
        for item in iterable:
            self.items_list.append(item)

    __update = update   # private copy of original update() method


# method name differs from field name only by capitalization
# ===========================================================

# Not compliant
class SomeClass:
    lookUp = False
    def lookup():       # Non-compliant; method name differs from field name only by capitalization
        pass

# Compliant Solution

class SomeClassOK:
    lookUp = False
    def getLookUp():
        pass
# naming conventions on classes, methods and fields


class bad_class_name(object):
    """
    python:S116: Field names should comply with a naming convention (minor)
    python:S117: Local variable and function parameter names should comply with a naming convention (minor)
    python:S100: Method names should comply with a naming convention
    """

    MyField = 1         # NOK (python:S116)
    my_field = 2        # OK

    MY_CLASS_CONST = 3

    def BadMethod_name(self,parameter_name_ok):  # NOK (python:S100)
        my_method_var = 1

    def good_method_name(self,BadParameterName):  # NOK (python:S117)
        my_method_var = 1

    def my_method (self,parameter_name_ok):
        L = 4
        l = 5
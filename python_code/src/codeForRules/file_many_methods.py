"""Checks on SonarQube rules """


class Class1(object):

    """ public class """
    class_variable = 0

    def __private_method(self):
        """ private method """
        a = 1

class Class2(object):
    """ public class """
    class_variable = 0

    def method_1(self):
        """ public  method """
        a = 1

    def __private_method_2(self):
        """ private method """
        a = 1



class Class3 (object):
    """ public class """
    class_variable = 0

    def method_1(self):
        """ public method """
        a = 1

    def method_2(self):
        """ public method """
        a = 1

    def method_3(self):
        """ public method """
        a = 1

    def method_4(self):
        """ public method """
        a = 1

    def method_5(self):
        """ public method """
        a = 1

    def method_6(self):
        """ public method """
        a = 1

    def method_7(self):
        """ public method """
        a = 1

    def method_8(self):
        """ public method """
        a = 1

    def method_9(self):
        """ public method """
        a = 1

    def method_10(self):
        """ public method """
        a = 1

    def method_11(self):
        """ public method """
        a = 1

    def method_12(self):
        """ public method """
        a = 1

    def method_13(self):
        """ public method """
        a = 1

    def method_14(self):
        """ public method """
        a = 1

    def method_15(self):
        """ public method """
        a = 1

    def method_16(self):
        """ public method """
        a = 1

    def method_17(self):
        """ public method """
        a = 1

    def method_18(self):
        """ public method """
        a = 1

    def method_19(self):
        """ public method """
        a = 1

    def method_20(self):
        """ public method """
        a = 1

    def method_21(self):
        """ public method """
        a = 1
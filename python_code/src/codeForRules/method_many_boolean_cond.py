"""Checks on SonarQube rules """

def func_1(c1,c2,c3,c4,c5,c6):
    """
    Pylint: R0916:Too many boolean expressions in if statement
    """

    if c1 or c2 or c3 or c4 or c5:
        a = 1

    if c1 or c2 or c3 or c4 or c5 or c6:
        a = 1

def func_2(c1,c2,c3,c4,c5):
    """
    Pylint: R0916:Too many boolean expressions in if statement
    """

    if (c1 or c2 or c3 or c4 or c5) and not (c1 and c2 and c3 and c4):
        a = 1


def func3(c1,c2,c3,c4,c5,c6):
    """
    Pylint: R0916:Too many boolean expressions in if statement
    """
    conds = [c1,c2,c3,c4,c5,c6]

    if any(conds) and not all(conds):
        a = 1
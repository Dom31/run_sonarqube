# Python module for checking the complexities (cyclomatic, cognitive)
# https://audiolion.github.io/python/2016/10/17/reducing-cyclomatic-complexity.html

class Bird_refactored(object):
  name = ''
  flightless = False
  extinct = False

  def get_speed(self):
    raise NotImplementedError

class Robin(Bird_refactored):
  name = 'Robin'

  def get_speed(self):
    return 14

class GoldFinch(Bird_refactored):
  name = 'Gold Finch'

  def get_speed(self):
    return 12

class Ostrich(Bird_refactored):
  name = 'Ostrich'
  flightless = True

  def get_speed(self):
    return 15

class Pterodactyl(Bird_refactored):
  name = 'Pterodactyl'
  extinct = True

  def get_speed(self):
    return -1
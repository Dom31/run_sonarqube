# python:S1142: Functions should not contain too many return statements
# # With the threshold of 3

def fun():          # Noncompliant as there are 4 return statements
  if condition1:
    return True
  elif condition2:
    return False
  else:
    return True
  return False

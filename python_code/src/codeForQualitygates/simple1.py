# Python module for checking the complexities (cyclomatic, cognitive)
# Source: https://en.wikipedia.org/wiki/Cyclomatic_complexity

class myclass (object):
    def func1(self):
        self.instance_variable1 = 7


def main():
    x = 1

def a():
    if x == 1:
        pass

def b():
    if x == 1:
        pass
    else:
        pass

def c():
    if x == 1:
        pass
    else:
        pass


if __name__ == "__main__":  # Ignored.
    main()

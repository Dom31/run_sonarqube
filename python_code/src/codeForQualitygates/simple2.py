
class Bird(object):
  name = ''
  flightless = False
  extinct = False

  def get_speed(self):

    if self.name == 'Ostrich':
      speed = 1
    elif self.name == 'Gold Finch':
      speed = 2
    elif self.name == 'Bluejay':
      speed = 3
    elif self.name == 'Robin':
      speed = 4
    elif self.name == 'Hummingbird':
      speed = 5

    return speed

  # cyclomatic_example.py
  import sys

def main():
  if len(sys.argv) > 1:  # 1
    filepath = sys.argv[1]
  else:
    print("Provide a file path")
    exit(1)
  if filepath:  # 2
    with open(filepath) as fp:  # 3
      for line in fp.readlines():  # 4
        if line != "\n":  # 5
          print(line, end="")

  def f():
    if (c1()):
      f1()
    else:
      f2()

    if (c2()):
      f3()
    else:
      f4()

  if __name__ == "__main__":  # Ignored.
    main()
